package com.database.migration.destinationdata;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class DestinationCust {

    private Long id;
    private String address;
    private String bvn;
    private String city;
    private Long consistencyVersion;
    private String correspondenceLanguage;
    private Long countryOfResidenceCode;
    private Long countryOfRiskCode;
    private String customerNumber;
    private Long customerTypeCode;
    private String emailAddress;
    private String fullName;
    private Long homeBranchCode;
    private String industry;
    private Long mailDeliveryBranchCode;
    private Long relationManager;
    private String receivePortMessage;
    private String managerId;
    private String nationality;
    private String phoneNumber;
    private String rcNoOrNatIdNo;
    private String routingNumber;
    private String shortName;
    private String status;
    private String swiftBicCode;
    private String taxIdNo;
    private String title;
    private String salutation;
}
