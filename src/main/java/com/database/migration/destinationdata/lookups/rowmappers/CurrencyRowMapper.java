package com.database.migration.destinationdata.lookups.rowmappers;

import com.database.migration.destinationdata.lookups.Currency;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CurrencyRowMapper implements RowMapper<Currency> {
    @Override
    public Currency mapRow(ResultSet resultSet, int i) throws SQLException {
        Currency currency = new Currency();
        currency.setCurrencyNumber(resultSet.getString("CURRENCY_NUMBER"));
        return currency;
    }
}
