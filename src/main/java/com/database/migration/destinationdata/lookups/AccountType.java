package com.database.migration.destinationdata.lookups;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class AccountType {

    private Long id;
    private String accountCat;
    private String accountSubCat;
    private Long bankId;
    private String code;
    private Long consistencyVersion;
    private Date effectiveDate;
    private String name;
    private String status;

}
