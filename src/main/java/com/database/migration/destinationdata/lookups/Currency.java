package com.database.migration.destinationdata.lookups;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Currency {

    private Long id;
    private String bidAble;
    private String code;
    private Long consistencyVersion;
    private String currencyNumber;
    private Long decimalPlaces;
    private String description;
    private String isoCode;
    private String leastDenomination;
    private String symbol;


}
