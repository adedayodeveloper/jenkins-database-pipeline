package com.database.migration.destinationdata.lookups.rowmappers;

import com.database.migration.destinationdata.lookups.CustomerType;
import com.database.migration.sourcedata.Customer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerTypeRowMapper implements RowMapper<CustomerType> {


    @Override
    public CustomerType mapRow(ResultSet resultSet, int i) throws SQLException {
        CustomerType customerType = new CustomerType();
        customerType.setId(resultSet.getLong("ID"));
        return customerType;
    }
}
