package com.database.migration.destinationdata.lookups;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class CustomerType {
    private Long id;
    private Long bankId;
    private String category;
    private String code;
    private Long consistencyVersion;
    private String description;
    private Date effectiveDate;
    private String status;
    private String subCat;

}
