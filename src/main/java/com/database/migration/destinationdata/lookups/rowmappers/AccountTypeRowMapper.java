package com.database.migration.destinationdata.lookups.rowmappers;

import com.database.migration.destinationdata.lookups.AccountType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountTypeRowMapper implements RowMapper<AccountType> {


    @Override
    public AccountType mapRow(ResultSet resultSet, int i) throws SQLException {
        AccountType accountType = new AccountType();
        accountType.setId(resultSet.getLong("ID"));
        return accountType;
    }
}
