package com.database.migration.destinationdata.lookups.rowmappers;


import com.database.migration.destinationdata.lookups.Country;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryRowMapper implements RowMapper<Country> {


    @Override
    public Country mapRow(ResultSet resultSet, int i) throws SQLException {
        Country country = new Country();
        country.setNumericCode(resultSet.getString("NUMERIC_CODE"));
        return country;
    }



}
