package com.database.migration.destinationdata.lookups;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Country {

    private Long Id;
    private String isoCode;
    private String code;
    private Long consistencyVersion;
    private String name;
    private String numericCode;

}
