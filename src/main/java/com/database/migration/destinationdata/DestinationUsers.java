package com.database.migration.destinationdata;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class DestinationUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String address;
    private Long consistencyVersion;
    private String email;
    private String fullName;
    private String password;
    private String phoneNumber;
    private String profilePicture;
    private String status;
    private String userName;
    private Long branch;
    private Long role;
    private boolean onlineFlag;
    private String superUser;
    private boolean firstResetFlag;



}
