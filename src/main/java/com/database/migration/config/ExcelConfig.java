package com.database.migration.config;

import com.database.migration.sourcedata.Users;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelConfig {


    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
//    static String[] HEADERs = {"S/N", "FIRST_NAME", "LAST_NAME", "LOGIN_USER_ID", "EMAIL", "EMPLOYEE_NO", "BRANCH_CODE", "SEC_POLICY_NAME", "ROLE_NAME" };
//    static String SHEET = "User Role Matrix_updated";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) { return false; }
        return true;
    }



    public static List<Users> excelToTutorials(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();

            List<Users> user = new ArrayList<Users>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Users users = new Users();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale

                    Cell currentCell = cellsInRow.next();
                    currentCell.setCellType(CellType.STRING);

                    switch (cellIdx) {
                        case 0:
                            users.setId(currentCell.getStringCellValue());
                        case 1:
                            users.setFirstName(currentCell.getStringCellValue());
                            break;

                        case 2:
                            users.setLastName(currentCell.getStringCellValue());
                            break;

                        case 3:
                            users.setLoginUserId(currentCell.getStringCellValue());
                            break;

                        case 4:
                            users.setEmail(currentCell.getStringCellValue());

                        case 5:
                            users.setEmployeeNumber(currentCell.getStringCellValue());

                        case 6:
                            users.setBranchCode(currentCell.getStringCellValue());

                        case 7:
                            users.setSecPolicyName(currentCell.getStringCellValue());
                        case 8:
                            users.setRoleName(currentCell.getStringCellValue());
                            break;

                        default:
                            break;
                    }

                    cellIdx++;
                }

                user.add(users);
            }

            workbook.close();

            return user;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }
}

