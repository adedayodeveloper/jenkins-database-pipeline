package com.database.migration.sourcedata;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Account {

    private Long id;
    private String accountNumber;
    private String accountTypeCode;
    private String branchCode;
    private String currency;
    private String customerCode;
    private Date dateOpened;
    private Date effectiveDate;
    private String fullName;
    private String iban;
    private boolean multiCurrency;
    private String shortName;
    private String status;

}
