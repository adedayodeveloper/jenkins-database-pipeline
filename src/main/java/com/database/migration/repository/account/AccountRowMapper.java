package com.database.migration.repository.account;


import com.database.migration.sourcedata.Account;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountRowMapper implements RowMapper<Account> {
    @Override
    public Account mapRow(ResultSet resultSet, int i) throws SQLException {
        Account account = new Account();
        account.setAccountNumber(resultSet.getString("ACCOUNT_NO"));
        account.setAccountTypeCode(resultSet.getString("ACCOUNT_TYPE_CODE"));
        account.setBranchCode(resultSet.getString("BRANCH_CODE"));
        account.setCurrency(resultSet.getString("CURRENCY"));
        account.setCustomerCode(resultSet.getString("CUSTOMER_CODE"));
        account.setDateOpened(resultSet.getDate("DATE_OPENED"));
        account.setEffectiveDate(resultSet.getDate("EFFECTIVE_DATE"));
        account.setFullName(resultSet.getString("FULL_NAME"));
        account.setIban(resultSet.getString("I_BAN"));
        account.setMultiCurrency(resultSet.getBoolean("MULTI_CURRENCY"));
        account.setShortName(resultSet.getString("SHORT_NAME"));
        account.setStatus(resultSet.getString("STATUS"));
        return account;
    }


    public AccountRowMapper() {
    }
}
