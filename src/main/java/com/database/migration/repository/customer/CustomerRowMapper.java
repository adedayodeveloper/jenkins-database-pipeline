package com.database.migration.repository.customer;

import com.database.migration.sourcedata.Customer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRowMapper implements RowMapper<Customer> {

    @Override
    public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
        Customer customer = new Customer();
        customer.setAddress(resultSet.getString("ADDRESS"));
        customer.setBvn(resultSet.getString("BVN"));
        customer.setCity(resultSet.getString("CITY"));
        customer.setCorrespondenceLanguage(resultSet.getString("CORRESPONDENCE_LANGUAGE"));
        customer.setCountryOfResidenceCode(resultSet.getString("COUNTRY_OF_RESIDENCE_CODE"));
        customer.setCountryOfRiskCode(resultSet.getString("COUNTRY_OF_RISK_CODE"));
        customer.setCustomerNumber(resultSet.getString("CUSTOMER_NO"));
        customer.setCustomerTypeCode(resultSet.getString("CUSTOMER_TYPE_CODE"));
        customer.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
        customer.setFullName(resultSet.getString("FULL_NAME"));
        customer.setHomeBranchCode(resultSet.getString("HOME_BRANCH_CODE"));
        customer.setIndustry(resultSet.getString("INDUSTRY"));
        customer.setMailDeliveryBranchCode(resultSet.getString("MAIL_DELIVERY_BRANCH_CODE"));
        customer.setManagerId(resultSet.getString("MANAGER_ID"));
        customer.setNationality(resultSet.getString("NATIONALITY"));
        customer.setPhoneNumber(resultSet.getString("PHONE_NUMBER"));
        customer.setRcNoOrNatIdNo(resultSet.getString("RC_NO_OR_NAT_ID_NO"));
        customer.setRoutingNumber(resultSet.getString("ROUTING_NO"));
        customer.setShortName(resultSet.getString("SHORT_NAME"));
        customer.setStatus(resultSet.getString("STATUS"));
        customer.setSwiftBicCode(resultSet.getString("SWIFT_BICCODE"));
        customer.setTaxIdNo(resultSet.getString("TAX_ID_NO"));
        customer.setTitle(resultSet.getString("TITLE"));
        return customer;
    }

    public CustomerRowMapper() {
    }
}
