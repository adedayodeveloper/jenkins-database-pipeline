package com.database.migration.repository.user;

import com.database.migration.constants.CommonConstants;
import com.database.migration.destinationdata.DestinationCust;
import com.database.migration.destinationdata.DestinationUsers;
import com.database.migration.repository.BaseRepository;
import com.database.migration.sourcedata.Customer;
import com.database.migration.sourcedata.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Transactional
@Repository
public class UserRepository extends BaseRepository<Users, DestinationUsers> {

    @Autowired
    public UserRepository(JdbcTemplate sourceJdbcTemplate, @Qualifier(CommonConstants.DESTINATION_JDBC_TEMPLATE_BEAN_NAME)
            JdbcTemplate destinationJdbcTemplate) {
        super(sourceJdbcTemplate, destinationJdbcTemplate);
    }

    @Override
    public List<Users> retrieveDataFromSourceDatabase(int startFrom) {
        return null;
    }

    @Override
    public boolean saveDataToDestinationDatabase(List<DestinationUsers> migrationData) {
        try{

            String sql = "insert into USERS (ID," +
                    "EMAIL," +
                    "USERNAME," +
                    "BRANCH," +
                    "FULL_NAME) values (?,?,?,?,?)";

            super.getDestinationJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement pStmt, int branchIndx) throws SQLException {
                    int parameterIndx = 1;
                    pStmt.setLong(parameterIndx++,migrationData.get(branchIndx).getId());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getEmail());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getUserName());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getBranch());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getFullName());
                }
                @Override
                public int getBatchSize() {
                    return migrationData.size();
                }
            });
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
