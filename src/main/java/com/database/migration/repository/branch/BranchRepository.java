package com.database.migration.repository.branch;


import com.database.migration.constants.CommonConstants;
import com.database.migration.sourcedata.Branch;
import com.database.migration.destinationdata.DestinationBranch;
import com.database.migration.repository.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class BranchRepository extends BaseRepository<Branch, DestinationBranch> {


    @Autowired
    public BranchRepository(JdbcTemplate sourceJdbcTemplate, @Qualifier(CommonConstants.DESTINATION_JDBC_TEMPLATE_BEAN_NAME)
            JdbcTemplate destinationJdbcTemplate) {
        super(sourceJdbcTemplate, destinationJdbcTemplate);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Branch> retrieveDataFromSourceDatabase(int startFrom) {
        String sql = "select * from BRANCH where id >= " + startFrom + " order by id";
        List<Branch> branches = super.getSourceJdbcTemplate().query(sql, new BranchRowMapper());
        if (branches == null) {
            return new ArrayList<>();
        }
        return branches;
    }


    @Override
    public boolean saveDataToDestinationDatabase(List<DestinationBranch> migrationData) {
        try{

            String sql = "insert into BRANCH (ID," +
                    "CITY," +
                    "ADDRESS," +
                    "COLLATION_OFFICE_FLAG," +
                    "EMAIL_ADDRESS," +
                    "HEAD_OFFICE_FLAG," +
                    "NAME," +
                    "PHONE_NUMBER," +
                    "REGIONAL_OFFICE_FLAG," +
                    "REGION," +
                    "ZIP," +
                    "CODE) values (?,?,?,?,?,?,?,?,?,?,?,?)";

            super.getDestinationJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement pStmt, int branchIndx) throws SQLException {
                    int parameterIndx = 1;
                    pStmt.setLong(parameterIndx++,migrationData.get(branchIndx).getId());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getCity());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getAddress());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getCollationOfficeFlag());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getEmailAddress());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getHeadOfficeFlag());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getName());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getPhoneNumber());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getRegionalOfficeFlag());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getRegionCode());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getZip());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getCode());

                }

                @Override
                public int getBatchSize() {
                    return migrationData.size();
                }
            });
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
