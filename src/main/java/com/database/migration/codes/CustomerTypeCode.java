package com.database.migration.codes;

public enum CustomerTypeCode {
    Correspondent,
    Corporate;
}
