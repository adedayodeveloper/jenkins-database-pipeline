package com.database.migration.codes;

public enum AccountTypeCode {

    COM01,
    BCCA01,
    BCC,
    CIN,
    CCM,
    CBN,
    CON1,
    CUSFEM,
    CC1,
    REC,
    COLACT,
    CURR01,
    VAT,
    NOST,
    INC,
    CU1,
    CU2,
    DIS001,
    SCF001,
    EXP,
    PAY,
    CUR,
    COMM,
    CC2,
    BNF,
    KAR,
    UNC,
    CURP,
    BND,
    CUO,
    CUN,
    FUN;



}
