package com.database.migration.service;


import com.database.migration.codes.AccountTypeCode;
import com.database.migration.codes.CustomerTypeCode;
import com.database.migration.config.ExcelConfig;
import com.database.migration.destinationdata.DestinationUsers;
import com.database.migration.destinationdata.lookups.CustomerType;
import com.database.migration.repository.user.UserRepository;
import com.database.migration.sourcedata.Account;
import com.database.migration.sourcedata.Branch;
import com.database.migration.sourcedata.Customer;
import com.database.migration.destinationdata.DestinationAcct;
import com.database.migration.destinationdata.DestinationBranch;
import com.database.migration.destinationdata.DestinationCust;
import com.database.migration.repository.account.AccountRepository;
import com.database.migration.repository.branch.BranchRepository;
import com.database.migration.repository.customer.CustomerRepository;
import com.database.migration.sourcedata.Users;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class ServiceDataMigrator implements DataMigrator {


    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(ServiceDataMigrator.class);



    public void saveExcelFileDataToDatabase(MultipartFile file){
        try {
            List<Users> users = ExcelConfig.excelToTutorials(file.getInputStream());
            List<DestinationUsers> destUsers = new ArrayList<>();
            users.stream().forEach(user -> {
                DestinationUsers users1 = new DestinationUsers();
                users1.setId(Long.parseLong(generateNumbers(1000)));
                users1.setEmail(user.getEmail());
                users1.setUserName(user.getLoginUserId());
                users1.setBranch(Long.parseLong(user.getBranchCode()));
                users1.setFullName(user.getFirstName() + " " + user.getLastName());
                destUsers.add(users1);
            });
            boolean saveToDestination = userRepository.saveDataToDestinationDatabase(destUsers);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }


    private static String generateNumbers(int i) {
        Random rnd = new Random();
        return ("" + (rnd.nextInt(i) + i)).substring(1);
    }


    @Override
    public void branchDataMigrate() {
        System.out.println("<<<<<<<<<<< Migrating Branch Data >>>>>>>>>>>");
        List<Branch>  getAll = branchRepository.retrieveDataFromSourceDatabase(1);
        getAll.stream().forEach(data -> {
            data.setId(Long.parseLong(generateNumbers(1000)));
        });

        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonData = mapper.writeValueAsString(getAll);
            List<DestinationBranch> branchData = mapper.readValue(jsonData, new TypeReference<List<DestinationBranch>>() {});
            System.out.println(branchData.size() + " Values");
           boolean saveToDestination = branchRepository.saveDataToDestinationDatabase(branchData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void accountDataMigrate(){
        System.out.println("<<<<<<<<<<< Migrating Account Data >>>>>>>>>>>");
        List<Account>  getAll = accountRepository.retrieveDataFromSourceDatabase(1);
        getAll.stream().forEach(data -> {
            data.setId(Long.parseLong(generateNumbers(1000)));
            // LOOK UP METHODS
            accountLookUp(data.getAccountTypeCode());
            // CALL FROM DESTINATION SOURCE TO GET CORRECT VALUES
            Long accountTypeCode = accountRepository.retrieveAccountTypeCode(accountLookUp(data.getAccountTypeCode()));
            String currencyCode = accountRepository.currencyLookUp(data.getCurrency());
            logger.info("branch code {}", data.getBranchCode());
            Long branchId = accountRepository.findBranchId(data.getBranchCode());

            data.setAccountTypeCode(accountTypeCode.toString());
            data.setCurrency(currencyCode);
            data.setBranchCode(branchId.toString());
        });

        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonData = mapper.writeValueAsString(getAll);
            List<DestinationAcct> acctData = mapper.readValue(jsonData, new TypeReference<List<DestinationAcct>>() {});
            System.out.println(acctData.size() +" Values");
            boolean saveToDestination = accountRepository.saveDataToDestinationDatabase(acctData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void customerDataMigrate(){
        System.out.println("<<<<<<<<<<< Migrating Customer Data >>>>>>>>>>>");
        List<Customer>  getAll = customerRepository.retrieveDataFromSourceDatabase(1);
        getAll.stream().forEach(data -> {
            String countryCode = customerRepository.findCountryCode(data.getCountryOfResidenceCode());
            data.setId(Long.parseLong(generateNumbers(1000)));
            Long customerTypeCode = customerRepository.getCustomerTypeCode(customerLookUp(data.getCustomerTypeCode()).toString());

            data.setCountryOfResidenceCode(countryCode);
            data.setCountryOfRiskCode(countryCode);
            data.setCustomerTypeCode(customerTypeCode.toString());
        });

        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonData = mapper.writeValueAsString(getAll);
            List<DestinationCust> custData = mapper.readValue(jsonData, new TypeReference<List<DestinationCust>>() {});
            System.out.println(custData.size() +" Values");
            boolean saveToDestination = customerRepository.saveDataToDestinationDatabase(custData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    private CustomerTypeCode customerLookUp(String cusstomerTypeCode){
            CustomerTypeCode customerTypeCode = null;
        if (cusstomerTypeCode.equals("CORPORATE")){
            customerTypeCode = CustomerTypeCode.Corporate;
        } else if(cusstomerTypeCode.equals("INDIVIDUAL") || cusstomerTypeCode.equals("BANK")){
            customerTypeCode = CustomerTypeCode.Correspondent;
        }
        return customerTypeCode;
    }



    private AccountTypeCode accountLookUp(String acctTypeCode){
        AccountTypeCode newAcctTypeCode = null;
        if (acctTypeCode.equals("CA")){
            newAcctTypeCode = AccountTypeCode.CUR;
        }else if (acctTypeCode.equals("GL")) {
            newAcctTypeCode = AccountTypeCode.CBN;
        }
        return newAcctTypeCode;
    }


}
