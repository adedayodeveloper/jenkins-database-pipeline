package com.database.migration.service;

import com.database.migration.constants.DataMigrationType;

public interface DataMigrator {
    void branchDataMigrate();
    void accountDataMigrate();
    void customerDataMigrate();
}
