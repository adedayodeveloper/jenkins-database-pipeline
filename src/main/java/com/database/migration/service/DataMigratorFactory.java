package com.database.migration.service;


import com.database.migration.constants.DataMigrationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataMigratorFactory {


    @Autowired
    private DataMigrator branchDataMigrator;

    @Autowired
    private DataMigrator accountDataMigrator;

    @Autowired
    private DataMigrator customerDataMigrator;

    public DataMigrator createDataMigratorService(DataMigrationType migrationType) {
        switch (migrationType) {
            case Branch:
                return branchDataMigrator;
            case Account:
                return accountDataMigrator;
            case Customer:
                return customerDataMigrator;
            default:
                throw new IllegalArgumentException("Invalid Migration Type");
        }
    }
}
